with Ada.Numerics.Discrete_Random;
use Ada.Numerics;

package body Mazes.Algorithms is
   -- Process the grid row, by row.
   -- For each cell, we have an equal chance of linking
   -- to the cell to the North, or East.
   -- On the top row, we always link East.
   -- On the rightmost column, we always link North.
   procedure BinaryTree(G : in out Grid) is
      subtype NorthOrEast is Direction range North..East;
      package RandomDirection is new Discrete_Random(NorthOrEast);
      Generator : RandomDirection.Generator;
   begin
      -- Reseed the RNG based on the current time.
      RandomDirection.Reset(Generator);
      for Row in G'Range(RowDim) loop
         for Col in G'Range(ColDim) loop
            if Col < G'Last(ColDim) and Row < G'Last(RowDim) then
               Link(G, Row, Col, RandomDirection.Random(Generator));
            elsif Col < G'Last(ColDim) and Row = G'Last(RowDim) then
               -- Top row, link East
               Link(G, Row, Col, East);
            elsif Col = G'Last(ColDim) and Row < G'Last(RowDim) then
               -- Last column, link North
               Link(G, Row, Col, North);
            end if;
            -- NB. no link in the top right corner.
         end loop;
      end loop;
   end BinaryTree;

   procedure Sidewinder(G : in out Grid) is
      type Move is (Extend, Close, Pass);

      -- Grid dependent index types
      subtype ColIndex is Positive range G'Range(ColDim);
      subtype RowIndex is Positive range G'Range(RowDim);

      -- Constants to improve readability
      RightCol : constant Positive := G'Last(ColDim);
      TopRow   : constant Positive := G'Last(RowDim);

      -- Choses a move depending on the current position in the grid.
      function SelectMove(Row: RowIndex; Col: ColIndex) return Move is
         subtype MoveOptions is Move range Extend..Close;
         package RandomMove is new Discrete_Random(MoveOptions);
         MoveGenerator : RandomMove.Generator;
      begin
         RandomMove.Reset(MoveGenerator);
         if Col < RightCol and Row < TopRow then
            return RandomMove.Random(MoveGenerator);
         elsif Col < RightCol then
            return Extend; -- top row, can Extent.
         elsif Row < TopRow then
            return Close;  -- right column, can Close.
         else
            return Pass;   -- top right corner.
         end if;
      end SelectMove;

      -- Pick a random Run index between RunStart and RunEnd.
      function SelectIndex(RunStart, RunEnd: ColIndex) return ColIndex is
         subtype RunRange is ColIndex range RunStart..RunEnd;
         package RandomIndex is new Discrete_Random(RunRange);
         IndexGenerator : RandomIndex.Generator;
      begin
         RandomIndex.Reset(IndexGenerator);
         return RandomIndex.Random(IndexGenerator);
      end SelectIndex;

      -- Keeps track of where the current run started.
      RunStart : ColIndex;

   begin
      for Row in G'Range(RowDim) loop
         RunStart := 1; -- Reset the run for this new row.
         for Col in G'Range(ColDim) loop
            case SelectMove(Row, Col) is
               when Pass   => null;
               when Extend => Link(G, Row, Col, East);
               when Close  => 
                  Link(G, Row, SelectIndex(RunStart, Col), North);
                  if Col < RightCol then
                     RunStart := Col + 1;
                  end if;
            end case;
         end loop;
      end loop;
   end Sidewinder;

end Mazes.Algorithms;

