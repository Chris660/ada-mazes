with Ada.Text_IO;

package body Mazes.Text_IO is
   procedure Put(G: Grid) is
      HWall: String := "-+";
      HGap: String := " +";
   begin
      -- North to South, East to West
      for Row in reverse G'Range(RowDim) loop
         -- Top walls
         Ada.Text_IO.Put('+');
         for Col in G'Range(ColDim) loop
            Ada.Text_IO.Put(if G(Row, Col).Links(North) then HGap else HWall);
         end loop;
         Ada.Text_IO.New_Line;

         -- Middle walls
         Ada.Text_IO.Put('|');
         for Col in G'Range(ColDim) loop
            Ada.Text_IO.Put(if G(Row, Col).Links(East) then "  " else " |");
         end loop;
         Ada.Text_IO.New_Line;
      end loop;

      -- Bottom border
      Ada.Text_IO.Put('+');
      for Col in G'Range(ColDim) loop
         Ada.Text_IO.Put(HWall);
      end loop;
      Ada.Text_IO.New_Line;
   end Put;
end Mazes.Text_IO;

