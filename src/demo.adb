with Mazes;
with Mazes.Algorithms;
with Mazes.Text_IO;
use Mazes;

procedure Demo is
   M: Grid := Mazes.NewGrid(6, 8);
begin
   Algorithms.Sidewinder(M);
   Text_IO.Put(M);
end Demo;

