package body Mazes is
   OppositeDirection: constant array (Direction) of Direction := (
      North => South, South => North,
      East => West, West => East);

   function NewGrid(Rows, Cols: Positive) return Grid is
      G: Grid(1..Rows, 1..Cols);
   begin
      return G;
   end NewGrid;

   procedure Link(G: in out Grid;
                  Row, Col: Positive;
                  D: Direction) is
   begin
      case D is
         when East =>
            if Col < G'Last(ColDim) then
               G(Row, Col).Links(East) := true;
               G(Row, Col + 1).Links(West) := true;
            end if;
         when West =>
            if Col > G'First(ColDim) then
               G(Row, Col).Links(West) := true;
               G(Row, Col - 1).Links(East) := true;
            end if;
         when North =>
            if Row < G'Last(RowDim) then
               G(Row, Col).Links(North) := true;
               G(Row + 1, Col).Links(South) := true;
            end if;
         when South =>
            if Col > G'First(RowDim) then
               G(Row, Col).Links(South) := true;
               G(Row - 1, Col).Links(North) := true;
            end if;
      end case;
   end Link;
end Mazes;
