package Mazes is
   type Direction is (North, East, South, West);
   type LinkFlags is array(Direction) of Boolean;

   type Cell is record
      Links: LinkFlags := (Others => false);
   end record;

   RowDim : constant := 1;
   ColDim : constant := 2;

   type Grid is array(Positive range <>,
                      Positive range <>) of Cell;

   function NewGrid(Rows, Cols: Positive) return Grid;
   procedure Link(G: in out Grid; Row, Col: Positive; D: Direction);
end Mazes;
